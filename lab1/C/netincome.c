// Calculate the net income of the user based on their offer
// and the income tax of the state the offer is from.
// Basic error checking.

#include <stdio.h>

double calculate(double gross, double statetax);

int main() {
    double gross = 0.0;
    char statehastax = 'y';
    double statetax = 0.0;
    double net = 0.0;

    printf("What is your starting salary?\n");
    scanf("%lf", &gross);
    if (gross < 0) goto neg_error;
    printf("Does the state have income tax? (y/n)\n");
    scanf(" %c", &statehastax);
    if (statehastax == 'y') {
        printf("What is the income tax for the state (as a percentage)?\n");
        scanf(" %lf", &statetax);
        if (statetax < 0) goto neg_error;
    } else {
        statetax = 0.0;
    }

    net = calculate(gross, statetax);

    printf("Your net income is %.2f\n", net);
    return 0;

neg_error:
    printf("Input must be positive");
    return 1;
}

double calculate (double gross, double statetax) {
    double net = gross;
    // social security
    if (gross > 65000.0) {
        net = net - (65000.0 * .103);
    } else {
        net = net - (gross * .103);
    }
    // federal
    net = net - 3500.0;
    if (gross > 30000.0) {
        net = net - ((gross - 30000.0) * .28);
    }
    // state
    net = net - (gross * (statetax / 100));

    return net;
}
